@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    Registro não disponível, por favor contacte o administrador e solicite seu login.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
